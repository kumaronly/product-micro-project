package jdbcproject2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Logger;

public class CreatingDatabase {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Logger log=Logger.getLogger(CreatingDatabase.class.getName());
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306","root","root");
		Statement st=con.createStatement();
		st.execute("create database Advento");
//		System.out.println("data base is created ");
		log.info("data base is created ");
		con.close();
		
	}
}
