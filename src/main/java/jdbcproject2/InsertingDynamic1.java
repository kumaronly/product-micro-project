package jdbcproject2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.Scanner;

public class InsertingDynamic1 {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/School1","root","root");
		Statement st=con.createStatement();
		Scanner sc=new Scanner(System.in);
		while(true)
		{
			System.out.println("enter the id");
			int id=sc.nextInt();
			System.out.println("enter the name");
			String name=sc.next();
			System.out.println("enter the age");
			int age=sc.nextInt();
			System.out.println("enter the address");
			String address=sc.next();
//			String sqlQuery="insert into student values("+id+",'+name+")",id,name,age,address);
			st.execute("insert into student values("+id+",'"+name+"',"+age+",'"+address+"')");
			System.out.println("record inserted successfully");
			System.out.println("do you want add more yes or no ??????");
			String option=sc.next();
			if(option.equalsIgnoreCase("no"))
			{
				break;
			}
		}
		System.out.println("added all data");
		con.close();
		

	}
}
