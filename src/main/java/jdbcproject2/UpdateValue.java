package jdbcproject2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class UpdateValue {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/School1","root","root");
		Statement st=con.createStatement();
		st.executeUpdate("update student set age=50 where id=1");
		System.out.println("data updated");
		con.close();
	}
}
