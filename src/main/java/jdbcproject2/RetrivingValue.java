package jdbcproject2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

public class RetrivingValue {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/School1","root","root");
		Statement st =(Statement) con.createStatement();
		ResultSet rs=st.executeQuery("select * from Student");
		while(rs.next())
		{
			System.out.println("id:"+rs.getInt(1)+"... name "+rs.getString(2)+" age "+rs.getInt(3)+"address :"+rs.getString(4));
		}
//		System.out.println(rs);
		con.close();
	}
}
