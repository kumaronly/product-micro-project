package jdbcproject2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;


public class EstablishedConnection {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
//		load and resister the driver 
		Class.forName("com.mysql.cj.jdbc.Driver");
//		established connection between java and db
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306","root","root");
		System.out.println("connection established ........");
	}
}
