package jdbcproject2;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class InsertingValue {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection c=DriverManager.getConnection("jdbc:mysql://localhost:3306/School1","root","root");
		Statement st=c.createStatement();
		st.executeUpdate("insert into student values(1,'kumar',28,'odisha')");
		System.out.println("data is inserted");
		c.close();
	}
}
