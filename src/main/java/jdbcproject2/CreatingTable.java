package jdbcproject2;

import java.sql.Statement;
import java.util.logging.Logger;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;

public class CreatingTable {
	public static void main(String[] args) throws ClassNotFoundException, SQLException {
		Logger log=Logger.getLogger(CreatingTable.class.getName());
		Class.forName("com.mysql.cj.jdbc.Driver");
		Connection con=DriverManager.getConnection("jdbc:mysql://localhost:3306/School2","root","root");
		Statement st=con.createStatement();
		st.execute("create table Student(id integer, name varchar(45), age integer, address varchar(45))");
		System.out.println("table is created");
		log.info("table is created");
		con.close();
	}
}
